﻿using System;


namespace exercise_19
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            //Declare variables
            var num1 = 6;
            var num2 = 7;
            var num3 = 2;
            var num4 = 3;
            var num5 = 4;

            /*Display to screen equation and result -Even if variables are changed the equation will 
            still display correctly and the result will also be correct*/
            Console.WriteLine($"Qa. ({num1}+{num2}) x ({num4}-{num3}) = {(num1+num2)*(num4-num3)}");
            
            Console.WriteLine($"Qb. ({num1}x{num2}) + ({num4}x{num3}) = {(num1*num2)+(num4*num3)}");
            
            Console.WriteLine($"Qc. ({num1}x{num2}) + {num4} x {num3} = {(num1*num2)+num4*num3}");
            
            Console.WriteLine($"Qd. ({num4}x{num3}) x {num1} + {num2} = {(num4*num3)*num1+num2}");
            
            Console.WriteLine($"Qe. ({num4}x{num3} + {num2} + {num1} / {num3} = {(num4*num3)+num2+num1/num3}");
            
            Console.WriteLine($"Qf. {num1} + {num2} x {num4} - {num3} = {num1+num2*num4-num3}");
            
            Console.WriteLine($"Qg. {num4} x {num3} + ({num4}x{num3} = {num4*num3+(num4*num3)}");
            
            Console.WriteLine($"Qh. ({num1}x{num2}) x {num2} + {num1} = {(num1*num2)*num2+num1}");
            
            Console.WriteLine($"Qi. ({num3}x{num3} + {num3} x {num3} = {(num3*num3)+num3*num3}");
            
            Console.WriteLine($"Qj. {num4} x {num4} + ({num4}x{num4}) = {num4*num4+(num4*num4)}");
            
            Console.WriteLine($"Qk. ({num1}\xB2+{num2}) x {num4} + {num3} = {(Math.Pow(num1,2)+num2)*num4+num3}");
            
            Console.WriteLine($"Ql. ({num4}x{num3}) + {num4}\xB2 x {num3} = {(num4*num3)+Math.Pow(num4,2)*num3}");
            
            Console.WriteLine($"Qm. ({num1} x ({num2}+{num2})) / {num1} = {(num1*(num2+num2))/num1}");
            
            Console.WriteLine($"Qn. (({num3}+{num3}) + ({num3}x{num3})) = {((num3+num3)+num3*num3)}");
            
            Console.WriteLine($"Qo. {num5} x {num5} + ({num4}\xB2 x {num4}\xB2) = {num5*num5+(Math.Pow(num4,2)*Math.Pow(num4,2))}");
        
            Console.ReadKey();
            
        }
    }
}
